[![Netlify Status](https://api.netlify.com/api/v1/badges/d909a849-9cdc-44bc-883b-f7e78c29f793/deploy-status)](https://app.netlify.com/sites/cryptonomad/deploys)

Official website for Sahyadri OpenSource Community.  
All contribution to this website including contents are made through Pull Requests. If you are new to Git and GitHub and don't know how to submit a Pull Request(PR), please refer our friendly guide on submitting your first pull request at [gitme.js.org](https://gitme.js.org).
